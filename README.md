# Email sender and password generator

    * Email sender
        - Attach files
        - Attach images
        - Send email

    * Password generator
        - Generate password with a length between 15 and 60 characters
        - Copy the password into the clipboard

## Note

The pass generator will only copy the password into the clipboard on linux OS.
