import os


def clear_and_print(message: str) -> None:
    os.system("clear")
    print(message)


def validate_is_number(number: str, message: str) -> int:
    while not number.isdigit():
        clear_and_print(message)
        number = input("\n[-] Enter a valid number: ")

    return int(number)


def validate_length(length: int, message: str) -> int:
    while length <= 14 or length > 100:
        clear_and_print(message)
        length = validate_is_number(
            input("\n[-] Enter a valid length: "), message)

    return length
